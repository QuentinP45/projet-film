<?php
require_once 'models/connect.php';

/** Classe de gestion des films servant de modèle
*   à notre application avec des méthodes de type CRUD
*/
class Film {
	/** Objet contenant la connexion pdo à la BD */
	private static $connexion;

	/** Constructeur établissant la connexion */
	function __construct()
	{
		$dsn="mysql:dbname=".BASE.";host=".SERVER.";charset=utf8";
		try{
			self::$connexion=new PDO($dsn,USER,PASSWD);
		}
		catch(PDOException $e){
			printf("Échec de la connexion : %s\n", $e->getMessage());
			$this->connexion = NULL;
		}
	}

	function getAllFilms()
	{
		$sql=
			'SELECT *
			FROM film'
		;

		$data=self::$connexion->query($sql);

		return $data;
	}

	function getTitreFilm($idFilm)
	{
		$sql=
			'SELECT titre
			FROM film
			WHERE id=:idFilm'
		;

		$stmt=self::$connexion->prepare($sql);
		$stmt->bindParam(':idFilm',$idFilm);
		$stmt->execute();

		$film=$stmt->fetch(PDO::FETCH_OBJ);

		if($film) {
			return $film->titre;
		} else {
			return false;
		}
	}

	function getFilm($idFilm)
	{
		$sql=
			'SELECT *
			FROM film
			WHERE id=:idFilm'
		;

		$stmt=self::$connexion->prepare($sql);
		$stmt->bindParam(':idFilm',$idFilm);
		$stmt->execute();

		$film=$stmt->fetch(PDO::FETCH_OBJ);
		if ($film) {
			return $film;
		} else {
			return false;
		}
	}

	function supprimerFilm($idFilm)
	{
		$sql=
			'DELETE FROM film
			WHERE id=:idFilm'
		;

		$stmt=self::$connexion->prepare($sql);
		$stmt->bindParam(':idFilm',$idFilm);
		
		if($stmt->execute()) {
			return true;
		}
		return false;
	}

	function addFilm($titre,$dateSortie,$synopsys,$duree,$image)
	{

		$sql=
			'INSERT INTO film(titre,date_sortie,synopsys,duree,image)
			VALUES(:titre,:date_sortie,:synopsys,:duree,:image)'
		;

		$stmt=self::$connexion->prepare($sql);
		$stmt->bindParam(':titre',$titre);
		$stmt->bindParam(':date_sortie',$dateSortie);
		$stmt->bindParam(':synopsys',$synopsys);
		$stmt->bindParam(':duree',$duree);
		$stmt->bindParam(':image',$image);
		$stmt->execute();
	}

	function updateFilm($id,$titre,$dateSortie,$synopsys,$duree,$image)
	{
		$sql=
			'UPDATE film
			SET
				titre=:titre,
				date_sortie=:date_sortie,
				synopsys=:synopsys,
				duree=:duree,
				image=:image
			where id=:id'
		;

		$stmt=self::$connexion->prepare($sql);
		$stmt->bindParam(':titre',$titre);
		$stmt->bindParam(':date_sortie',$dateSortie);
		$stmt->bindParam(':synopsys',$synopsys);
		$stmt->bindParam(':duree',$duree);
		$stmt->bindParam(':image',$image);
		$stmt->bindParam(':id',$id);
		
		return $stmt->execute();
	}
}
