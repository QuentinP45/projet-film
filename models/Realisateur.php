<?php

/** Classe de gestion des realisateurs servant de modèle
*   à notre application avec des méthodes de type CRUD
*/
class Realisateur {
	/** Objet contenant la connexion pdo à la BD */
	private static $connexion;

	/** Constructeur établissant la connexion */
	function __construct()
	{
		$dsn="mysql:dbname=".BASE.";host=".SERVER.";charset=utf8";
		try{
			self::$connexion=new PDO($dsn,USER,PASSWD);
		}
		catch(PDOException $e){
			printf("Échec de la connexion : %s\n", $e->getMessage());
			$this->connexion = NULL;
		}
	}

	function getAllRealisateurs()
	{
		$sql=
			'SELECT *
			FROM realisateur'
		;

		$data=self::$connexion->query($sql);

		return $data;
	}

	function getRealisateur($idRealisateur)
	{
		$sql=
			'SELECT *
			FROM realisateur
			WHERE id=:idRealisateur'
		;

		$stmt=self::$connexion->prepare($sql);
		$stmt->bindParam(':idRealisateur',$idRealisateur);
		$stmt->execute();

		$realisateur=$stmt->fetch(PDO::FETCH_OBJ);
		if ($realisateur) {
			return $realisateur;
		} else {
			return false;
		}
	}

	function supprimerRealisateur($idRealisateur)
	{
		$sql=
			'DELETE FROM realisateur
			WHERE id=:idRealisateur'
		;

		$stmt=self::$connexion->prepare($sql);
		$stmt->bindParam(':idRealisateur',$idRealisateur);
		$stmt->execute();
	}

	function addRealisateur($prenom,$nom,$biographie)
	{

		$sql=
			'INSERT INTO realisateur(prenom,nom,biographie)
			VALUES(:prenom,:nom,:biographie)'
		;

		$stmt=self::$connexion->prepare($sql);
		$stmt->bindParam(':prenom',$prenom);
		$stmt->bindParam(':nom',$nom);
		$stmt->bindParam(':biographie',$biographie);
		$stmt->execute();
	}

	function updateRealisateur($id,$nom,$prenom,$biographie)
	{
		$sql=
			'UPDATE realisateur
			SET
				nom=:nom,
				prenom=:prenom,
				biographie=:biographie
			where id=:id'
		;

		$stmt=self::$connexion->prepare($sql);
		$stmt->bindParam(':id',$id);
		$stmt->bindParam(':nom',$nom);
		$stmt->bindParam(':prenom',$prenom);
		$stmt->bindParam(':biographie',$biographie);
		$stmt->execute();

		return $stmt->execute();
	}
}