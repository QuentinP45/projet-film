<?php

/** Classe de gestion des FilmRealisateur servant de modèle
*   à notre application avec des méthodes de type CRUD
*/
class FilmRealisateur {
	/** Objet contenant la connexion pdo à la BD */
	private static $connexion;

	/** Constructeur établissant la connexion */
	function __construct()
	{
		$dsn="mysql:dbname=".BASE.";host=".SERVER;
		try{
			self::$connexion=new PDO($dsn,USER,PASSWD);
		}
		catch(PDOException $e){
			printf("Échec de la connexion : %s\n", $e->getMessage());
			$this->connexion = NULL;
		}
	}

	function getFilmRealisateurs($idFilm)
	{
		$sql=
			'SELECT nom,prenom
			FROM film_realisateur
			INNER JOIN realisateur
				ON film_realisateur.id_realisateur = realisateur.id  
			WHERE film_realisateur.id_film=:idFilm';

		$stmt=self::$connexion->prepare($sql);
		$stmt->bindParam(':idFilm',$idFilm);
		$stmt->execute();

		return $stmt->fetchAll(PDO::FETCH_OBJ);
	}
}