<?php
// routing (lecture des actions)
require_once 'controllers/filmController.php';
require_once 'controllers/filmRealisateurController.php';
require_once 'controllers/realisateurController.php';

include 'vendor/autoload.php';
// le dossier ou on trouve les templates
$loader = new Twig\Loader\FilesystemLoader('templates');
// initialiser l'environement Twig
$twig = new Twig\Environment($loader);

$action=$_GET['action'] ?? 'films';

switch ($action){
    case 'films':
        // var_dump($_GET['ajouter']);die;
        liste_film_action($twig);
        break;
    case 'details':
        $idFilm=$_GET['film'] ?? 1;
        details_film_action($twig,$idFilm);
        break;
    case 'realisateurs':
        liste_realisateur_action($twig);
        break;
    case 'details-realisateur':
        $idRealisateur=$_GET['realisateur'] ?? 1;
        details_realisateur_action($twig,$idRealisateur);
        break;
    case 'supprimer-film':
        if (!empty($_GET['id'])) {
            supprimer_film_action($twig,$_GET['id']);
        }
        break;
    case 'creer-film':
        creer_film_action($twig);
        break;
    case 'supprimer-realisateur':
        if (!empty($_GET['id'])) {
            supprimer_realisateur($_GET['id']);
            liste_realisateur_action($twig);
        }
    case 'creer-realisateur':
        creer_realisateur_action($twig);
        break;
    case 'editer-film':
        editer_film_action($twig);
        break;
    case 'editer-realisateur':
        editer_realisateur_action($twig);
        break;
}