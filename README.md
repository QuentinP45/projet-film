# Projet-film
**Projet:** PHP, HTML et CSS | Git & GitLab
**Groupe:** Quentin Picart, Julien Moulin, Nils Quintown
**Cadre:** projet pour le cours DUT Informatique AS Web
## Programmes nécessaires pour l'installation
- Un éditeur de code
- Composer
- Git
- Environnement de développement web 
- Conseillers: Lamp (Linux), Wamp (Windows), Mamp (Mac)

**_Attention_**
> Pour l'installation en exemple, nous partons du principe que les programmes nécessaires sont installés et accessibles depuis votre terminal.
Les commandes de terminal respectent la syntaxe bash, elle sont à adapter au type de terminal utilisé.
Les commandes de l'installation pour l'enrichissement de la base de donnée sont fonction du système de gestion de base de donné utilisé.
Les informations entre chevrons servent d'exemples et doivent être remplacées par vos informations.
La système de gestion de base de données utilisé par l'application est MySql, elle n'est pas modifiable par un fichier de configuration. Il est recommandé pour pouvoir utiliser l'application d'installer l'un des environnement conseillés ou d'avoir à disposition le système de gestion de base données MySql.

## Installation du projet
- Lancer l'environnement de développement
- Ouvrir un terminal afin d'exécuter les commandes suivantes à la racine du répertoire de travail créé par l'environnement installé (ex: www)
- Lancer un éditeur de code
- Ouvrir un navigateur

### 1. Dans le terminal
- `git clone https://gitlab.com/QuentinP45/projet-film.git` afin de récupérer le dossier projet-film depuis Gitlab dans le répertoire de travail
- `cd projet-film` pour se déplacer dans le répertoire projet-film, positionné automatiquement sur la branche master locale du projet
- `composer install` afin d'installer la dépendance Twig (moteur de template utilisé par l'application)
- `mysql -h <server> -u <user> -p` pour se connecter au système de gestion de base de données après avoir renseigné le mot de passe
- `create database <DBname>;` pour créer une base de donnée de nom: <DBname>
- `use <DBname>` afin de sélectionner la base de données de nom <DBname>
- `source db_projet.sql` pour enrichir la base de données

### 2. Dans l'éditeur de code
- Ouvrir le répertoire projet-film pour afficher l'arborescence du projet
- Renommer le fichier connect.env.php situé dans le répertoire models en connect.php
- Ouvrir le fichier connect.php
- Modifier USER, PASSWD, SERVER et BASE par les informations correspondantes pour permettre à l'application de se connecter à la base de données <DBname>

### 3. Dans le navigateur
- Aller à l'url du localhost offert par l'environnement utilisé
- Double-cliquer sur le projet projet-film

À l'issue de ces étapes, l'application s'affiche sur le navigateur et est fonctionnelle !