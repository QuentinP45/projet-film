DROP TABLE film;
DROP TABLE realisateur;
DROP TABLE film_realisateur;

CREATE TABLE `film` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `titre` varchar(50) NOT NULL,
  `date_sortie` date NOT NULL,
  `synopsys` text NOT NULL,
  `duree` time NOT NULL,
  `image` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `film`
--

INSERT INTO `film` (`id`, `titre`, `date_sortie`, `synopsys`, `duree`, `image`) VALUES
(1, 'Inception', '2010-07-21', 'Dom Cobb est un voleur expérimenté dans l\'art périlleux de `l\'extraction : sa spécialité consiste à s\'approprier les secrets les plus précieux d\'un individu, enfouis au plus profond de son subconscient, pendant qu\'il rêve et que son esprit est particulièrement vulnérable. Très recherché pour ses talents dans l\'univers trouble de l\'espionnage industriel, Cobb est aussi devenu un fugitif traqué dans le monde entier. Cependant, une ultime mission pourrait lui permettre de retrouver sa vie d\'avant.', '02:28:00', 'https://m.media-amazon.com/images/M/MV5BMjAxMzY3NjcxNF5BMl5BanBnXkFtZTcwNTI5OTM0Mw@@._V1_UX182_CR0,0,182,268_AL_.jpg'),
(2, 'Interstellar', '2014-11-05', 'Dans un futur proche, la Terre est de moins en moins accueillante pour l\'humanité qui connaît une grave crise alimentaire. Le film raconte les aventures d\'un groupe d\'explorateurs qui utilise une faille récemment découverte dans l\'espace-temps afin de repousser les limites humaines et partir à la conquête des distances astronomiques dans un voyage interstellaire.', '02:49:00', 'https://m.media-amazon.com/images/M/MV5BZjdkOTU3MDktN2IxOS00OGEyLWFmMjktY2FiMmZkNWIyODZiXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_UX182_CR0,0,182,268_AL_.jpg'),
(3, 'Sin city', '2005-06-01', 'Sin City est une ville infestée de criminels, de flics ripoux et de femmes fatales. Hartigan s\'est juré de protéger Nancy, une strip-teaseuse qui l\'a fait craquer. Marv, un marginal brutal mais philosophe, part en mission pour venger la mort de son unique véritable amour, Goldie. Dwight est l\'amant secret de Shellie.', '02:27:00', 'https://m.media-amazon.com/images/M/MV5BODZmYjMwNzEtNzVhNC00ZTRmLTk2M2UtNzE1MTQ2ZDAxNjc2XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_UX182_CR0,0,182,268_AL_.jpg'),
(4, 'Pulp fiction', '1994-10-26', 'L\'odyssée sanglante et burlesque de petits malfrats dans la jungle de Hollywood à travers trois histoires qui s\'entremêlent. Dans un restaurant, un couple de jeunes braqueurs, Pumpkin et Yolanda, discutent des risques que comporte leur activité. Deux truands, Jules Winnfield et son ami Vincent Vega, qui revient d\'Amsterdam, ont pour mission de récupérer une mallette au contenu mystérieux et de la rapporter à Marsellus Wallace.', '02:58:00', 'https://m.media-amazon.com/images/M/MV5BNGNhMDIzZTUtNTBlZi00MTRlLWFjM2ItYzViMjE3YzI5MjljXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UY268_CR1,0,182,268_AL_.jpg'),
(5, 'Alita : Battle Angel', '2019-02-13', 'Lorsqu\'Alita se réveille sans aucun souvenir de qui elle est, dans un futur qu\'elle ne reconnaît pas, elle est accueillie par Ido, un médecin qui comprend que derrière ce corps de cyborg abandonné, se cache une jeune femme au passé extraordinaire. Ce n\'est que lorsque les forces obscures et corrompues qui gèrent la ville d\'Iron City se lancent à sa poursuite qu\'Alita découvre la clé de son passé - elle a des capacités de combat uniques, que ceux qui détiennent le pouvoir veulent.', '02:02:00', 'https://m.media-amazon.com/images/M/MV5BNzVhMjcxYjYtOTVhOS00MzQ1LWFiNTAtZmY2ZmJjNjIxMjllXkEyXkFqcGdeQXVyNTc5OTMwOTQ@._V1_UX182_CR0,0,182,268_AL_.jpg'),
(6, 'Jurassic Park', '1993-10-20', 'Jurassic Park ou Le Parc jurassique au Québec et au Nouveau-Brunswick, est une série cinématographique américaine de science-fiction, faisant partie d\'une franchise notamment composée de six films.', '02:07:00', 'https://m.media-amazon.com/images/M/MV5BMjM2MDgxMDg0Nl5BMl5BanBnXkFtZTgwNTM2OTM5NDE@._V1_UX182_CR0,0,182,268_AL_.jpg'),
(7, 'Avatar', '2019-12-16', 'Malgré sa paralysie, Jake Sully, un ancien marine immobilisé dans un fauteuil roulant, est resté un combattant au plus profond de son être. Il est recruté pour se rendre à des années-lumière de la Terre, sur Pandora, où de puissants groupes industriels exploitent un minerai rarissime destiné à résoudre la crise énergétique sur Terre.', '02:42:00', 'https://m.media-amazon.com/images/M/MV5BMTYwOTEwNjAzMl5BMl5BanBnXkFtZTcwODc5MTUwMw@@._V1_UX182_CR0,0,182,268_AL_.jpg'),
(8, 'Titanic', '1998-01-07', 'Le RMS Titanic est un paquebot transatlantique britannique qui fait naufrage dans l\'océan Atlantique Nord en 1912 à la suite d\'une collision avec un iceberg, lors de son voyage inaugural de Southampton à New York.', '03:30:00', 'https://m.media-amazon.com/images/M/MV5BMDdmZGU3NDQtY2E5My00ZTliLWIzOTUtMTY4ZGI1YjdiNjk3XkEyXkFqcGdeQXVyNTA4NzY1MzY@._V1_UX182_CR0,0,182,268_AL_.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `film_realisateur`
--

CREATE TABLE `film_realisateur` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `id_film` tinyint(3) UNSIGNED NOT NULL,
  `id_realisateur` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `film_realisateur`
--

INSERT INTO `film_realisateur` (`id`, `id_film`, `id_realisateur`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 2),
(4, 3, 3),
(5, 3, 4),
(6, 4, 3),
(7, 5, 4),
(8, 6, 5),
(9, 7, 6),
(10, 8, 6);

--
-- Structure de la table `realisateur`
--

CREATE TABLE `realisateur` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `biographie` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `realisateur`
--

INSERT INTO `realisateur` (`id`, `nom`, `prenom`, `biographie`) VALUES
(1, 'Nolan', 'Christopher', 'Christopher Nolan est un réalisateur, scénariste, monteur et producteur de cinéma britannico-américain, né le 30 juillet 1970 à Westminster. Il se fait connaître à la fin des années 1990 par un premier long métrage, Following, le suiveur, tourné en noir et blanc.'),
(2, 'Miller', 'Frank', 'Frank Miller, né le 27 janvier 1957 à Olney dans le Maryland, est un auteur de bandes dessinées américain, également scénariste de films et réalisateur. Il a adapté certaines de ses œuvres pour le cinéma.'),
(3, 'Tarentino', 'Quentin', 'Quentin Tarantino, né le 27 mars 1963 à Knoxville dans le Tennessee, est un réalisateur, scénariste, producteur et acteur américain.'),
(4, 'Rodriguez', 'Robert', 'Robert Rodríguez, né le 20 juin 1968 à San Antonio, Texas, est un réalisateur et musicien américain d\'origine mexicaine. Diplômé de l\'université du Texas à Austin, il est connu pour tourner des films à petit budget qui rencontrent souvent un grand succès public et dans lesquels il occupe de nombreux « postes ».'),
(5, 'Spielberg', 'Steven', 'Steven Spielberg /ˈstiːvən ˈspiːlbɝɡ/ est un réalisateur, scénariste et producteur de cinéma américain, né le 18 décembre 1946 à Cincinnati. Issu de la deuxième génération du Nouvel Hollywood dans les années 1970, il réalise le premier blockbuster de l\'histoire du cinéma, Les Dents de la mer.'),
(6, 'Cameron', 'James', 'James Francis Cameron [ d͡ʒeɪmz ˈfɹænsɪs ˈkæmɹən], né le 16 août 1954 à Kapuskasing est un réalisateur, scénariste, producteur et explorateur de fonds marins canadien qui habite aux États-Unis.');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `film`
--
ALTER TABLE `film`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ind_uni_titre_date_sortie` (`titre`,`date_sortie`);

--
-- Index pour la table `film_realisateur`
--
ALTER TABLE `film_realisateur`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ind_uni_id_film_id_realisateur` (`id_film`,`id_realisateur`),
  ADD KEY `fk_id_realisateur` (`id_realisateur`);



--
-- Index pour la table `realisateur`
--
ALTER TABLE `realisateur`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ind_uni_nom_prenom` (`nom`,`prenom`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `film`
--
ALTER TABLE `film`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `film_realisateur`
--
ALTER TABLE `film_realisateur`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `realisateur`
--
ALTER TABLE `realisateur`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `film_realisateur`
ALTER TABLE `film_realisateur`
  ADD CONSTRAINT `fk_id_film` FOREIGN KEY (`id_film`) REFERENCES `film` (`id`)
    ON DELETE CASCADE,
  ADD CONSTRAINT `fk_id_realisateur` FOREIGN KEY (`id_realisateur`) REFERENCES `realisateur` (`id`)
    ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
