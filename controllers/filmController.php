<?php
require_once 'models/Film.php';
require_once 'models/FilmRealisateur.php';

function liste_film_action($twig,$notification=null)
{
    $film=new Film();
    $films=$film->getAllFilms();
    $template=$twig->load('liste_films.html.twig');
    echo $template->render([
        'films'=>$films,
        'notification'=>$notification
    ]);
}

function details_film_action($twig,$idFilm)
{
    $film=new Film();
    $dataFilm=$film->getFilm($idFilm);

    $filmRealisateur=new FilmRealisateur();
    $dataFilmRealisateur=$filmRealisateur->getFilmRealisateurs($idFilm);

    $template=$twig->load('details_film.html.twig');
    echo $template->render([
        'film'=>$dataFilm,
        'realisateurs'=>$dataFilmRealisateur
    ]);    
}

function supprimer_film_action($twig,$idFilm)
{
    $film=new Film();
    $titreFilm=$film->getTitreFilm($idFilm);
    $notification=[];
    
    // film présent dans la base
    if($titreFilm != false) {
        if($film->supprimerFilm($idFilm)) {
            $notification['statut']='notification-valide';
            $notification['message']="Le film $titreFilm a été supprimé de la liste";
        } else {
            $notification['statut']='notification-invalide';
            $notification['message']="Le film $titreFilm n'a pas été supprimé de la liste";
        }
    } else {
    // film inexistant

    }
    
    // affichage liste films avec notification statut supression
    liste_film_action($twig,$notification);
}

function creer_film_action($twig)
{
    $template=$twig->load('creer_film.html.twig');

    if (!empty($_GET['ajouter'])) {
        if(!empty($_GET['titre'])
            && !empty($_GET['date_sortie'])
            && !empty($_GET['synopsys'])
            && !empty($_GET['duree'])
            && !empty($_GET['image'])) {

            $titre=$_GET['titre'];
            $dateSortie=$_GET['date_sortie'];
            $synopsys=$_GET['synopsys'];
            $duree=$_GET['duree'];
            $image=$_GET['image'];

            $film=new Film;
            $film->addFilm($titre,$dateSortie,$synopsys,$duree,$image);
        }
    }

    echo $template->render();
}

function editer_film_action($twig) {
    $film=new Film;

    // récupère film d'ID $id
    if (!empty($_GET['id'])) {
        $id=$_GET['id'];

        $dataFilm=$film->getFilm($id);

        $notification=[];

        // film existant
        if ($dataFilm) {
            $template=$twig->load('editer_film.html.twig');

            // garde en session les data du film en base
            session_start();
            $_SESSION['titre']=$dataFilm->titre;
            $_SESSION['date_sortie']=$dataFilm->date_sortie;
            $_SESSION['synopsys']=$dataFilm->synopsys;
            $_SESSION['duree']=$dataFilm->duree;
            $_SESSION['image']=$dataFilm->image;

            // READ film
            if (!empty($_GET['task']) && $_GET['task']=='read') {
                echo $template->render([
                    'film'=>$dataFilm
                ]);
            // UPDATE film
            } elseif (!empty($_GET['task'])
                && $_GET['task']=='update') {

                $titre=$_GET['titre'];
                $dateSortie=$_GET['date_sortie'];
                $synopsys=$_GET['synopsys'];
                $duree=$_GET['duree'];
                $image=$_GET['image'];

                // verifie que les champs de films ne sont pas null et qu'il y a eu modification par l'utilisateur
                if ($titre != null && $titre != $_SESSION['titre'] 
                    || $dateSortie != null && $dateSortie != $_SESSION['date_sortie'] 
                    || $synopsys != null && $synopsys != $_SESSION['synopsys']
                    || $duree != null && $duree != $_SESSION['duree']
                    || $image != null && $image != $_SESSION['image']) {
                        
                    // update valide
                    if ($film->updateFilm($id,$titre,$dateSortie,$synopsys,$duree,$image)) {
                        $notification['statut']='notification-valide';
                        $notification['message']="Le film $titre a été édité";
                    // update invalide
                    } else {
                        $notification['statut']='notification-invalide';
                        $notification['message']="Le film $titre n'a pas été édité, réessayez plus tard !";
                    }

                    echo $template->render([
                        'film'=>$film->getFilm($id),
                        'notification'=>$notification
                    ]);
                // au moins un champ null ou pas de modification de l'utilisateur 
                } else {
                    $notification['statut']='notification-invalide';
                    $notification['message']="Le film $titre n'a pas été édité";

                    echo $template->render([
                        'film'=>$film->getFilm($id),
                        'notification'=>$notification
                    ]);
                }
            }
        // film inexistant
        } else {
            // redirection
            header('Location: index.php');
        }         
    // pas d'id
    } else {
        // redirection
        header('Location: index.php');
    }
}
