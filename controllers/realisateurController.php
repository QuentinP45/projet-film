<?php
require_once 'models/Realisateur.php';
require_once 'models/FilmRealisateur.php';

function liste_realisateur_action($twig)
{
    $realisateur=new Realisateur();
    $realisateurs=$realisateur->getAllRealisateurs();
    $template=$twig->load('liste_realisateurs.html.twig');
    echo $template->render([
        'realisateurs'=>$realisateurs
    ]);
}

function details_realisateur_action($twig,$idRealisateur)
{
    $realisateur=new Realisateur();
    $dataRealisateur=$realisateur->getRealisateur($idRealisateur);

    
    $template=$twig->load('details_realisateur.html.twig');
    echo $template->render([
        'realisateur'=>$dataRealisateur
    ]);    
}
function supprimer_realisateur($idRealisateur)
{
    $realisateur=new Realisateur();
    $realisateur->supprimerRealisateur($idRealisateur);
}

function creer_realisateur_action($twig)
{
    $template=$twig->load('creer_realisateur.html.twig');

    if (!empty($_GET['ajouter'])) {
        if(!empty($_GET['prenom'])
            && !empty($_GET['nom'])
            && !empty($_GET['biographie'])) {

            $prenom=$_GET['prenom'];
            $nom=$_GET['nom'];
            $biographie=$_GET['biographie'];

            $realisateur=new Realisateur;
            $realisateur->addRealisateur($prenom,$nom,$biographie);
        }
    }

    echo $template->render();
}

function editer_realisateur_action($twig) {
    $realisateur=new Realisateur;

    // récupère realisateur d'ID $id
    if (!empty($_GET['id'])) {
        $id=$_GET['id'];

        $dataRealisateur=$realisateur->getRealisateur($id);
        $notification=[];

        // realisateur existant
        if ($dataRealisateur) {

            $template=$twig->load('editer_realisateur.html.twig');

            // garde en session les data du realisateur en base
            session_start();
            $_SESSION['nom']=$dataRealisateur->nom;
            $_SESSION['prenom']=$dataRealisateur->prenom;
            $_SESSION['biographie']=$dataRealisateur->biographie;

            // READ realisateur
            if (!empty($_GET['task']) && $_GET['task']=='read') {
                echo $template->render([
                    'realisateur'=>$dataRealisateur
                ]);
            // UPDATE realisateur
            } elseif (!empty($_GET['task'])
                && $_GET['task']=='update') {

                $nom=$_GET['nom'];
                $prenom=$_GET['prenom'];
                $biographie=$_GET['biographie'];

                // verifie que les champs de realisateurs ne sont pas null et qu'il y a eu modification par l'utilisateur
                if ($nom != null && $nom != $_SESSION['nom'] 
                    || $prenom != null && $prenom != $_SESSION['prenom'] 
                    || $biographie != null && $biographie != $_SESSION['biographie']) {
                    
                        
                    // update valide
                    if ($realisateur->updateRealisateur($id,$nom,$prenom,$biographie)) {
                        $notification['statut']='notification-valide';
                        $notification['message']="Le realisateur $nom a été édité";
                    // update invalide
                    } else {

                        $notification['statut']='notification-invalide';
                        $notification['message']="Le realisateur $nom n'a pas été édité, réessayez plus tard !";
                    }

                    echo $template->render([
                        'realisateur'=>$realisateur->getRealisateur($id),
                        'notification'=>$notification
                    ]);
                // au moins un champ null ou pas de modification de l'utilisateur 
                } else {
                    $notification['statut']='notification-invalide';
                    $notification['message']="Le realisateur $nom n'a pas été édité";

                    echo $template->render([
                        'realisateur'=>$realisateur->getRealisateur($id),
                        'notification'=>$notification
                    ]);
                }
            }
        // realisateur inexistant
        } else {
            // redirection
           var_dump("coucou1");
           die;
            header('Location: index.php');
        }         
    // pas d'id
    } else {
        var_dump("coucou2");
           die;
        // redirection
        header('Location: index.php');
    }
}